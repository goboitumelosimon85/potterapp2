package com.example.potterheadapp.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.potterheadapp.R
import com.example.potterheadapp.model.CharacterModel
import kotlinx.android.synthetic.main.row_charater_viewholder.view.*

class CharacterAdapter : RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    private val characterList = mutableListOf<CharacterModel>()

    fun setCharacterList(characterList: List<CharacterModel>) {
        this.characterList.clear()
        this.characterList.addAll(characterList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.row_charater_viewholder,
            parent, false)
        return CharacterViewHolder(view)
    }

    override fun getItemCount() = characterList.size

    @SuppressLint("DefaultLocale")
    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        val character = characterList[position]
        with(holder) {
            characterNameTextView.text = String.format(characterNameTextView.context.getString(
                R.string.name_placeholder
            ), character.name)
            characterRoleTextView.text = String.format(characterRoleTextView.context.getString(
                R.string.role_placeholder
            ), character.role?.capitalize() ?: "Unknown")
            characterHouseTextView.text = String.format(characterHouseTextView.context.getString(
                R.string.house_placeHolder
            ), character.house?.capitalize() ?: "Unknown")
            characterSchoolTextView.text = String.format(characterSchoolTextView.context.getString(
                R.string.school_placeHolder
            ), character.school?.capitalize() ?: "Unknown")
            orderOfThePhoenixTextView.text = String.format(orderOfThePhoenixTextView.context.getString(
                R.string.orderOfThePhoenix_placeholder
            ), character.orderOfThePhoenix)
            DeathEaterTextView.text = String.format(DeathEaterTextView.context.getString(
                R.string.death_eater_placeHolder
            ), character.deathEater)
        }
    }

    inner class CharacterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val characterNameTextView: TextView = itemView.character_name_textview
        val characterRoleTextView: TextView = itemView.character_role_textview
        val characterHouseTextView: TextView = itemView.character_house_textview
        val characterSchoolTextView: TextView = itemView.character_school_textview
        val orderOfThePhoenixTextView: TextView = itemView.orderOfThePhoenix_textview
        val DeathEaterTextView: TextView = itemView.deathEater_textview
    }
}