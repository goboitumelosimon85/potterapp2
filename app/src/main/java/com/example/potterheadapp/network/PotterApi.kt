
package com.example.potterheadapp.network

import com.example.potterheadapp.model.CharacterModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface PotterApi {

// charater needs Api key
  @GET("v1/characters")
  fun getCharacters(@Query("key") key: String = API_KEY): Call<List<CharacterModel>>
}

//my api below with back slash for $
val API_KEY = "\$2a\$10\$1JEnmtEF417yBaFZcr51qukRjaKv8d5toEG5DKP/IUZWIVwfsaF7y"
