package com.example.potterheadapp.model

import com.google.gson.annotations.SerializedName

data class CharacterModel(
    @SerializedName("name")
    val name: String,
    @SerializedName("role")
    val role: String?,
    @SerializedName("house")
    val house: String?,
    @SerializedName("school")
    val school: String?,
    @SerializedName("orderOfThePhoenix")
    val orderOfThePhoenix: Boolean,
    @SerializedName("eathEater")
    val deathEater: Boolean

)